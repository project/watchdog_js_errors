/**
 * @file JavaScript for Watchdog js errors.
 */

(function($) {
  Drupal.behaviors.watchdog_js_errors = {
    attach: function(context, settings) {
      $(window).bind('beforeunload', function() {
        if (errors.length > 0) {
          Drupal.watchdog_js_errors.save(errors);
        }
      });

      $(document).bind('watchdog_js_errors_save', function(event, response) {
        errors = [];
      });
    }
  },

  Drupal.watchdog_js_errors = {} || Drupal.watchdog_js_errors;

  /**
   * Save errors in js.
   *
   * @param array errors
   *   Error messages for save in watchdog.
   */
  Drupal.watchdog_js_errors.save = function(errors, callback) {
    $.ajax({
      url: settings.basePath + "watchdog_js_errors",
      data: {
        errors: errors,
      },
      type: "POST",
      dataType: 'json',
      sucess : function(response) {
        $(document).trigger('watchdog_js_errors_save', response);
      }
    });
  };

})(jQuery);

// Save all errors in this variable.
var errors = [];

// Error event.
window.onerror = function(msg, url, line, col, error) {
  // Push errors in Array.
  errors.push({message: msg, url: url, line: line});
}
